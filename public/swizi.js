var isIOS = false
var pluginIOSBridge

if (typeof plugins === 'undefined') {
    if (window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.plugins) {
        pluginIOSBridge = window.webkit.messageHandlers.plugins
        isIOS = true
    }
}

var swizi = {
    version: 2.0,
    addListenerMulti: function(element, eventNames, listener) {
        var events = eventNames.split(' ')
        for (var i = 0, iLen = events.length; i < iLen; i++) {
            element.addEventListener(events[i], listener, false)
        }
    },
    removeListenerMulti: function(element, eventNames, listener) {
        var events = eventNames.split(' ')
        for (var i = 0, iLen = events.length; i < iLen; i++) {
            element.removeEventListener(events[i], listener, false)
        }
    },
    promises: {},
    generateUUID: function() {
        var d = new Date().getTime()
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = ((d + Math.random() * 16) % 16) | 0
            d = Math.floor(d / 16)
            return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16)
        })
        return uuid
    },
    resolvePromise: function(promiseId, data, error) {
        if (error) {
            swizi.promises[promiseId].reject(data)
        } else {
            swizi.promises[promiseId].resolve(data)
        }
        delete swizi.promises[promiseId]
    },
    call: function(method, args) {
        var promise = new Promise(function(resolve, reject) {
            var promiseId = swizi.generateUUID()
            swizi.promises[promiseId] = { resolve: resolve, reject: reject }
            try {
                pluginIOSBridge.postMessage({ promiseId: promiseId, method: method, args: args || [] })
            } catch (exception) {
                alert(exception)
            }
        })
        return promise
    },
    isFullscreen: false,
    isPluginBridgeReady: function() {
        if (typeof pluginIOSBridge != 'undefined' || typeof plugins != 'undefined') return true
        else return false
    },
    log: function(type, msg) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'log', args: [type, msg] })
            } else {
                console[type](msg)
            }
        } else {
            return null
        }
    },
    navigateTo: function(filename, type, animated) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({
                    method: 'showPageWithPresentationTypeAnimated',
                    args: [filename, type, animated],
                })
            } else {
                plugins.showPageWithPresentationTypeAnimated(filename, type, animated)
            }
        } else {
            return null
        }
    },
    back: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'goBackToPreviousPage' })
            } else {
                plugins.goBackToPreviousPage()
            }
        } else {
            return null
        }
    },
    getPlatform: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getPlatform', null).then(function(datas) {
                    return datas
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getPlatform())
                })
                q = q.then(function(datas) {
                    return datas
                })
                return q
            }
        } else {
            return null
        }
    },
    getPlatformVersion: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getPlatformVersion', null).then(function(datas) {
                    return JSON.parse(datas)
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getPlatformVersion())
                })
                q = q.then(function(datas) {
                    return JSON.parse(datas)
                })
                return q
            }
        } else {
            return null
        }
    },
    getUser: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getUser', null).then(function(datas) {
                    return JSON.parse(datas)
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getUser())
                })
                q = q.then(function(datas) {
                    return JSON.parse(datas)
                })
                return q
            }
        } else {
            return null
        }
    },
    getToken: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getToken', null).then(function(datas) {
                    return datas
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getToken())
                })
                q = q.then(function(datas) {
                    return datas
                })
                return q
            }
        } else {
            return null
        }
    },
    getUserPhoto: function(userID) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                var args = []
                if (userID) {
                    args.push(userID)
                }
                pluginIOSBridge.postMessage({ method: 'getUserPhoto', args: args })
            } else {
                plugins.getUserPhoto(userID)
            }
        } else {
            return null
        }
    },
    getUserLocation: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                var args = []
                pluginIOSBridge.postMessage({ method: 'getUserLocation', args: args })
            } else {
                plugins.getUserLocation()
            }
        } else {
            return null
        }
    },
    getColor: function(colorName) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getApplicationColorWithID', [colorName]).then(function(datas) {
                    if (datas) {
                        return JSON.parse(datas)
                    } else {
                        return null
                    }
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getApplicationColorWithID(colorName))
                })
                q = q.then(function(datas) {
                    if (datas) {
                        return JSON.parse(datas)
                    } else {
                        return null
                    }
                })
                return q
            }
        } else {
            return null
        }
    },
    setFullscreen: function() {
        if (swizi.isPluginBridgeReady()) {
            swizi.isFullscreen = !swizi.isFullscreen
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'useFullScreen', args: [swizi.isFullscreen] })
            } else {
                plugins.useFullScreen(swizi.isFullscreen)
            }
        } else {
            return null
        }
    },
    setTitle: function(title) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'setViewTitle', args: [title] })
            } else {
                plugins.setViewTitle(title)
            }
        } else {
            return null
        }
    },
    setStatusBarLight: function(isLight) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'setStatusBarLight', args: [isLight] })
            } else {
                plugins.setStatusBarLight(isLight)
            }
        } else {
            return null
        }
    },
    setStatusColor: function(color) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'setStatusColor', args: [color] })
            } else {
                plugins.setStatusColor(color)
            }
        } else {
            return null
        }
    },
    readQRCode: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'readQRCode' })
            } else {
                plugins.readQRCode()
            }
        } else {
            return null
        }
    },
    takePicture: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'takePicture' })
            } else {
                plugins.takePicture()
            }
        } else {
            return null
        }
    },
    getGenericActions: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getGenericActions', null).then(function(datas) {
                    return JSON.parse(datas)
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getGenericActions())
                })
                q = q.then(function(datas) {
                    return JSON.parse(datas)
                })
                return q
            }
        } else {
            return null
        }
    },
    performGenericAction: function(actionID) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'performGenericAction', args: [actionID] })
            } else {
                plugins.performGenericAction(actionID)
            }
        } else {
            return null
        }
    },
    getItems: function(label) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getAPIKeyValuesWithLabel', [label]).then(function(datas) {
                    return JSON.parse(datas)
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getAPIKeyValuesWithLabel(label))
                })
                q = q.then(function(datas) {
                    return JSON.parse(datas)
                })
                return q
            }
        } else {
            return null
        }
    },
    getItemForKey: function(key, label) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getAPIKeyValueFromKeyWithLabel', [key, label]).then(function(datas) {
                    return datas
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getAPIKeyValueFromKeyWithLabel(key, label))
                })
                q = q.then(function(datas) {
                    return datas
                })
                return q
            }
        } else {
            return null
        }
    },
    setItemForKey: function(item, key, label) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'setAPIKeyValueWithLabel', args: [key, item, label] })
            } else {
                plugins.setAPIKeyValueWithLabel(key, item, label)
            }
        } else {
            return null
        }
    },
    removeItemForKey: function(key) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'deleteAPIKey', args: [key] })
            } else {
                plugins.deleteAPIKey(key)
            }
        } else {
            return null
        }
    },
    getFiles: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getFiles', null).then(function(datas) {
                    return JSON.parse(datas)
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getFiles())
                })
                q = q.then(function(datas) {
                    return JSON.parse(datas)
                })
                return q
            }
        } else {
            return null
        }
    },
    getFile: function(URL) {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('getFile', [URL]).then(function(datas) {
                    return JSON.parse(datas)
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.getFile(URL))
                })
                q = q.then(function(datas) {
                    return JSON.parse(datas)
                })
                return q
            }
        } else {
            return null
        }
    },
    activateGPSTracking: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'activateGPSTracking' })
            } else {
                plugins.activateGPSTracking()
            }
        } else {
            return null
        }
    },
    deactivateGPSTracking: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                pluginIOSBridge.postMessage({ method: 'deactivateGPSTracking' })
            } else {
                plugins.deactivateGPSTracking()
            }
        } else {
            return null
        }
    },
    isGPSTrackingEnabled: function() {
        if (swizi.isPluginBridgeReady()) {
            if (isIOS) {
                return swizi.call('isGPSTrackingEnabled', null).then(function(datas) {
                    return JSON.parse(datas)
                })
            } else {
                var q = new Promise(function(resolve, reject) {
                    resolve(plugins.isGPSTrackingEnabled())
                })
                q = q.then(function(datas) {
                    return JSON.parse(datas)
                })
                return q
            }
        } else {
            return null
        }
    },
    onEvent: function(cb) {
        swizi.addListenerMulti(document, 'gamoEvent swiziEvent', cb)
    },
    removeEvent: function(cb) {
        swizi.removeListenerMulti(document, 'gamoEvent swiziEvent', cb)
    },
}

var gamo = swizi

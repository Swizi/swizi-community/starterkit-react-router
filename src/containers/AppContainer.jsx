import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { withRouter, Switch } from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { childFactoryCreator, getPathDepth, PropsRoute } from '../utils/router'
import * as AuthActions from '../redux/actions/auth'
import * as VisitActions from '../redux/actions/visit'
import resizeImage from 'resize-image'

import Home from '../components/Home/Home'
import Information from '../components/Information/Information'
import IncidentList from '../components/IncidentList/IncidentList'
import Incident from '../components/Incident/Incident'
import Summary from '../components/Summary/Summary'

class AppContainer extends React.Component {
    state = {
        prevDepth: getPathDepth(this.props.location),
        startTime: null,
        incidents: [],
        endTime: null,
        alleeVisited: null,
        isTakingPicture: '',
        mainPicuture: null,
        incidentPhotos: [],
        alleName: '',
    }

    componentWillMount() {
        swizi.setFullscreen()
    }
    componentDidMount() {
        swizi.getItemForKey('api-key', 'ms-forms-access').then(api => {
            swizi.getItemForKey('secret-key', 'ms-forms-access').then(secret => {
                swizi.getUser().then(user => {
                    this.props.authActions.getCurrentUser(user, api, secret)
                })
            })
        })
        swizi.onEvent(event => {
            switch (event.detail.type) {
            case 'takePicture':
                swizi.getFile(event.detail.value).then(data => {
                    var img = new Image()
                    img.onload = () => {
                        var thumbnailTemp = resizeImage.resize(img, 1000, 1000, resizeImage.JPEG)
                        if (this.state.isTakingPicture === 'main') this.setState({ mainPicuture: thumbnailTemp })
                        else this.setState({ incidentPhotos: [...this.state.incidentPhotos, thumbnailTemp] })
                    }
                    img.src = 'data:image/jpeg;base64,' + data.file
                })
                break
            default:
            }
        })
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ prevDepth: getPathDepth(this.props.location) })
        if (nextProps.authData.user.success && !this.props.authData.user.success) {
            this.props.visitActions.getPendingVisits(nextProps.authData.user.data.ESO)
            this.props.visitActions.getIncidentCategories()
        }
        if (
            nextProps.visitsData.created &&
            !nextProps.visitsData.creating &&
            !(this.props.visitsData.created && this.props.visitsData.created)
        )
            this._handleReset()
    }

    _initVisit = () => {
        this.props.history.push('/information')
    }
    _startVisit = () => {
        this.props.history.push('/information/incident')
    }
    _newIncident = () => {
        this.setState({ incidentsPhotos: [] }, () => this.props.history.push('/information/incident/new'))
    }
    _endVisit = () => {
        this.props.history.push('/information/incident/summary')
    }
    _handleBackToIncidents = () => {
        this.setState({ incidentPhotos: [] }, () => this.props.history.goBack())
    }
    _handleReset = () => {
        this.props.history.push('/')
        this.setState({
            startTime: null,
            incidents: [],
            endTime: null,
            alleeVisited: '',
            alleName: '',
            isTakingPicture: '',
            mainPicuture: null,
            incidentPhotos: [],
        })
    }
    _handleTakingMainPicture = () => {
        this.setState({ isTakingPicture: 'main' }, () => swizi.takePicture())
    }
    _handleTakingIncidentPicture = () => {
        this.setState({ isTakingPicture: 'incident' }, () => swizi.takePicture())
    }
    _handleStartVisit = (alleeVisited, startTime, alleName) => {
        this.setState({ startTime, alleeVisited, alleName }, () => this._startVisit())
    }
    _handleIncidentCreation = (dateCreation, photos, infos, description) => {
        let incident = {
            dateCreation,
            photos: this.state.incidentPhotos,
            infos,
            description,
            state: 'open',
        }
        let incidents = [...this.state.incidents, incident]
        this.setState({ incidents, incidentPhotos: [] }, () => this.props.history.goBack())
    }
    _handleVisitCreation = endTime => {
        let visit = {
            ESO: this.props.authData.user.data.ESO,
            id_IPAT: this.state.alleeVisited,
            incidents: JSON.stringify(this.state.incidents),
            photo: this.state.mainPicuture,
            date_start: this.state.startTime,
            date_end: endTime,
            state: 'open',
            site: this.state.alleName,
        }
        this.props.visitActions.addNewVisit(visit)
    }
    render() {
        return (
            <TransitionGroup
                childFactory={childFactoryCreator(
                    getPathDepth(this.props.location) - this.state.prevDepth >= 0 ? 'pageSliderLeft' : 'pageSliderRight'
                )}
            >
                <CSSTransition
                    key={this.props.location.pathname}
                    timeout={600}
                    classNames={
                        getPathDepth(this.props.location) - this.state.prevDepth >= 0
                            ? 'pageSliderLeft'
                            : 'pageSliderRight'
                    }
                    mountOnEnter={true}
                    unmountOnExit={true}
                >
                    <Switch location={this.props.location}>
                        <PropsRoute
                            path="/"
                            exact
                            component={Home}
                            authData={this.props.authData}
                            _initVisit={this._initVisit}
                        />
                        <PropsRoute
                            path="/information"
                            exact
                            route={getPathDepth(this.props.location) - this.state.prevDepth}
                            component={Information}
                            visitDate={this.state.visitDate}
                            visitsData={this.props.visitsData}
                            selectedAllee={this.state.alleeVisited}
                            mainPicuture={this.state.mainPicuture}
                            _handleReset={this._handleReset}
                            _handleTakingMainPicture={this._handleTakingMainPicture}
                            _handleStartVisit={this._handleStartVisit}
                        />
                        <PropsRoute
                            path="/information/incident"
                            exact
                            component={IncidentList}
                            incidents={this.state.incidents}
                            _newIncident={this._newIncident}
                            _endVisit={this._endVisit}
                        />
                        <PropsRoute
                            path="/information/incident/new"
                            exact
                            component={Incident}
                            categories={this.props.visitsData.visits.incidentCategories}
                            incidentPhotos={this.state.incidentPhotos}
                            _handleIncidentCreation={this._handleIncidentCreation}
                            _handleTakingIncidentPicture={this._handleTakingIncidentPicture}
                            _handleBackToIncidents={this._handleBackToIncidents}
                        />
                        <PropsRoute
                            path="/information/incident/summary"
                            exact
                            component={Summary}
                            startTime={this.state.startTime}
                            endTime={this.state.endTime}
                            alleeVisited={this.state.alleeVisited}
                            alleName={this.state.alleName}
                            incidents={this.state.incidents}
                            _handleVisitCreation={this._handleVisitCreation}
                            visitsData={this.props.visitsData}
                            _handleReset={this._handleReset}
                        />
                    </Switch>
                </CSSTransition>
            </TransitionGroup>
        )
    }
}

const mapStateToProps = ({ authReducer, visitReducer }) => ({
    authData: authReducer,
    visitsData: visitReducer,
})

const mapDispatchToProps = dispatch => ({
    authActions: bindActionCreators(AuthActions, dispatch),
    visitActions: bindActionCreators(VisitActions, dispatch),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppContainer))

AppContainer.propTypes = {
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    authData: PropTypes.object.isRequired,
    authActions: PropTypes.object.isRequired,
    visitsData: PropTypes.object.isRequired,
    visitActions: PropTypes.object.isRequired,
}

import { combineReducers } from 'redux'

import { authReducer } from './actions/auth'
import { visitReducer } from './actions/visit'

export default combineReducers({
    authReducer,
    visitReducer,
})

import * as actionTypes from '../constants/actionTypes'

/*
 * reducer
 */

const INITIAL_STATE = {
    visits: {
        data: null,
        filteredData: null,
        loading: false,
        success: false,
        incidentCategories: null,
        creating: false,
        created: false,
    },
}

export function visitReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
    case actionTypes.VISITS_FETCHING:
        return {
            ...state,
            visits: { ...state.visits, loading: true },
        }
    case actionTypes.VISITS_FETCHED:
        let filteredData = []
        action.visits.forEach(visit => {
            if (visit.lodgement_gender === 'ALLEE') filteredData.push(visit)
        })
        return {
            ...state,
            visits: { ...state.visits, loading: false, success: true, data: action.visits, filteredData },
        }
    case actionTypes.INCIDENT_CATEGORIES_FETCHED:
        return { ...state, visits: { ...state.visits, incidentCategories: action.categories } }
    case actionTypes.VISITS_CREATING:
        return { ...state, creating: true }
    case actionTypes.VISITS_CREATED:
        return { ...state, created: true, creating: false }
    default:
        return state
    }
}

/*
 * action creators
 */

export function getPendingVisits(ESO) {
    return dispatch => {
        dispatch(fetchingVisits())
        fetch('https://ms-forms-dev.eu-gb.mybluemix.net/api/form/record/find', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'x-api-key': 'kfXSWG3coDsdvRWlIzsFz7QQ9ETGD5B8cEhtaSC1',
                'x-secret-key': 'hSNuLgALbRexeeN5XIKAXTgQF',
            }),
            body: JSON.stringify({
                formName: 'IPAT',
                criteria: {
                    num_ESO: ESO,
                },
            }),
        })
            .then(r => r.json())
            .then(response => {
                if (response.success) {
                    dispatch(fetchedVisits(response.data))
                }
            })
    }
}
function fetchingVisits() {
    return { type: actionTypes.VISITS_FETCHING }
}
function fetchedVisits(visits) {
    console.log(visits)
    return { type: actionTypes.VISITS_FETCHED, visits }
}
export function getIncidentCategories() {
    return dispatch => {
        fetch('https://ms-forms-dev.eu-gb.mybluemix.net/api/form/record/find', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'x-api-key': 'kfXSWG3coDsdvRWlIzsFz7QQ9ETGD5B8cEhtaSC1',
                'x-secret-key': 'hSNuLgALbRexeeN5XIKAXTgQF',
            }),
            body: JSON.stringify({
                formName: 'Incident_Category',
                criteria: {},
            }),
        })
            .then(r => r.json())
            .then(response => {
                if (response.success) {
                    dispatch(fetchedCategories(response.data))
                }
            })
    }
}
function fetchedCategories(categories) {
    console.log(categories)
    return { type: actionTypes.INCIDENT_CATEGORIES_FETCHED, categories }
}
export function addNewVisit(visit) {
    console.log(visit)
    return dispatch => {
        dispatch(visitCreating())
        fetch('https://ms-forms-dev.eu-gb.mybluemix.net/api/form/record/add', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'x-api-key': 'kfXSWG3coDsdvRWlIzsFz7QQ9ETGD5B8cEhtaSC1',
                'x-secret-key': 'hSNuLgALbRexeeN5XIKAXTgQF',
            }),
            body: JSON.stringify({
                formName: 'Visits',
                fields: visit,
            }),
        })
            .then(r => r.json())
            .then(response => {
                console.log(response)
                if (response.success) {
                    dispatch(visitCreated())
                }
            })
    }
}
function visitCreating() {
    return { type: actionTypes.VISITS_CREATING }
}
function visitCreated() {
    return { type: actionTypes.VISITS_CREATED }
}

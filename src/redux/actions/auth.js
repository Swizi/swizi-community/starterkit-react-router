import * as actionTypes from '../constants/actionTypes'

/*
 * reducer
 */

const INITIAL_STATE = {
    user: { data: null, loading: false, success: false },
}

export function authReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
    case actionTypes.USER_FETCHING:
        return {
            ...state,
            user: { ...state.user, loading: true },
        }
    case actionTypes.USER_FETCHED:
        return {
            ...state,
            user: { ...state.user, loading: false, success: true, data: action.user },
        }
    default:
        return state
    }
}

/*
 * action creators
 */

export function getCurrentUser(user, api, secret) {
    return dispatch => {
        dispatch(fetchingUser())
        fetch('https://ms-forms-dev.eu-gb.mybluemix.net/api/form/record/find', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'x-api-key': api,
                'x-secret-key': secret,
            }),
            body: JSON.stringify({
                formName: 'Gardian',
                criteria: {
                    mail: user.login,
                },
            }),
        })
            .then(r => r.json())
            .then(response => {
                if (response.success) {
                    dispatch(fetchedUser(response.data[0]))
                }
            })
    }
}
function fetchingUser() {
    return { type: actionTypes.USER_FETCHING }
}
function fetchedUser(user) {
    return { type: actionTypes.USER_FETCHED, user }
}

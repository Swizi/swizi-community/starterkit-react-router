import React, { Component } from 'react'
import './Summary.less'
import PropTypes from 'prop-types'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faRecap from '@fortawesome/fontawesome-free-solid/faSortAmountDown'
import faHoursStart from '@fortawesome/fontawesome-free-solid/faHourglassStart'

import { Timeline, TimelineEvent } from 'react-event-timeline'

import moment from 'moment'
import Loading from '../Loading/Loading'
moment.locale('fr')

const startIcon = (
    <svg width="16" height="16" fill="rgba(0,0,0,0.5)" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
        <path d="M1536 128q0 261-106.5 461.5t-266.5 306.5q160 106 266.5 306.5t106.5 461.5h96q14 0 23 9t9 23v64q0 14-9 23t-23 9h-1472q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h96q0-261 106.5-461.5t266.5-306.5q-160-106-266.5-306.5t-106.5-461.5h-96q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h1472q14 0 23 9t9 23v64q0 14-9 23t-23 9h-96zm-128 0h-1024q0 66 9 128h1006q9-61 9-128zm0 1536q0-130-34-249.5t-90.5-208-126.5-152-146-94.5h-230q-76 31-146 94.5t-126.5 152-90.5 208-34 249.5h1024z" />
    </svg>
)
const incidentIcon = (
    <svg width="16" height="16" fill="rgba(0,0,0,0.5)" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
        <path d="M1088 1248v224q0 26-19 45t-45 19h-256q-26 0-45-19t-19-45v-224q0-26 19-45t45-19h256q26 0 45 19t19 45zm30-1056l-28 768q-1 26-20.5 45t-45.5 19h-256q-26 0-45.5-19t-20.5-45l-28-768q-1-26 17.5-45t44.5-19h320q26 0 44.5 19t17.5 45z" />
    </svg>
)
const endIcon = (
    <svg width="16" height="16" fill="rgba(0,0,0,0.5)" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
        <path d="M1536 128q0 261-106.5 461.5t-266.5 306.5q160 106 266.5 306.5t106.5 461.5h96q14 0 23 9t9 23v64q0 14-9 23t-23 9h-1472q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h96q0-261 106.5-461.5t266.5-306.5q-160-106-266.5-306.5t-106.5-461.5h-96q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h1472q14 0 23 9t9 23v64q0 14-9 23t-23 9h-96zm-534 708q77-29 149-92.5t129.5-152.5 92.5-210 35-253h-1024q0 132 35 253t92.5 210 129.5 152.5 149 92.5q19 7 30.5 23.5t11.5 36.5-11.5 36.5-30.5 23.5q-137 51-244 196h700q-107-145-244-196-19-7-30.5-23.5t-11.5-36.5 11.5-36.5 30.5-23.5z" />
    </svg>
)

const cardIncidentStyle = { backgroundColor: 'var(--red)', color: 'rgba(0,0,0,0.5)', fontWeight: 'bold' }
const cardVisitStyle = { backgroundColor: 'var(--green)', color: 'rgba(0,0,0,0.6)', fontWeight: 'bold' }

export default class Summary extends Component {
    state = { dateEnd: Date.now() }
    componentDidMount() {
        this.interval = setInterval(() => this.setState({ dateEnd: Date.now() }), 1000)
        console.log(this.props)
    }
    componentWillUnmount() {
        clearInterval(this.interval)
    }
    componentWillReceiveProps(nextProps) {}
    render() {
        return (
            <div className="mainWrapper">
                <div className="new-visit-container column  centered aligned ">
                    <div className="header-container enterDown">
                        <img src="assets/images/logo_simple.png" alt="" />
                        <img
                            className="arrow-back"
                            src="https://png.icons8.com/ios/50/000000/left.png"
                            onClick={() => this.props.history.goBack()}
                        />
                    </div>
                    <span className="label-legend arriveSlide">
                        <FontAwesomeIcon icon={faRecap} className="icon-label" />Récapitulatif de la visite :
                    </span>
                    <div className="timeline-wrapper">
                        <Timeline className="test">
                            <TimelineEvent
                                title={moment(this.props.startTime).format('LT') + ' - Début de la visite'}
                                icon={startIcon}
                                container="card"
                                cardHeaderStyle={cardVisitStyle}
                                bubbleStyle={{
                                    backgroundColor: 'var(--green)',
                                    color: 'rgba(0,0,0,0.5)',
                                    border: '2px solid rgba(0, 0, 0, 0.5)',
                                }}
                            >
                                <div className="column aligned centered">
                                    <span
                                        style={{
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                        }}
                                    >
                                        {this.props.alleName}
                                    </span>
                                    <p className="card-description" style={{ textAlign: 'center' }}>
                                        {this.props.alleeVisited.label}
                                    </p>
                                </div>
                            </TimelineEvent>
                            {this.props.incidents.map((incident, index) => {
                                return (
                                    <TimelineEvent
                                        key={index}
                                        title={moment(incident.dateCreation).format('LT') + ' - Incident'}
                                        icon={incidentIcon}
                                        container="card"
                                        cardHeaderStyle={cardIncidentStyle}
                                        bubbleStyle={{
                                            backgroundColor: 'var(--red)',
                                            color: 'rgba(0,0,0,0.5)',
                                            border: '2px solid rgba(0, 0, 0, 0.5)',
                                        }}
                                    >
                                        <div className="column aligned centered">
                                            <span style={{ fontWeight: 'bold', textAlign: 'center' }}>
                                                {incident.infos.type + ' - ' + incident.infos.nature}
                                            </span>
                                            <p style={{ textAlign: 'center' }} className="card-description">
                                                {incident.description}
                                            </p>
                                        </div>
                                    </TimelineEvent>
                                )
                            })}
                            <TimelineEvent
                                title={moment(this.state.dateEnd).format('LTS') + ' - Fin de la visite'}
                                icon={endIcon}
                                container="card"
                                cardHeaderStyle={cardVisitStyle}
                                bubbleStyle={{
                                    backgroundColor: 'var(--green)',
                                    color: 'rgba(0,0,0,0.5)',
                                    border: '2px solid rgba(0, 0, 0, 0.5)',
                                }}
                            >
                                <div className="column aligned centered">
                                    <span
                                        style={{
                                            fontWeight: 'bold',
                                            textAlign: 'center',
                                        }}
                                    >
                                        Fin de la visite
                                    </span>
                                </div>
                            </TimelineEvent>
                        </Timeline>
                        <div
                            style={{
                                display: 'flex',
                                minWidth: '95vw',
                                justifyContent: 'center',
                            }}
                        >
                            {!this.props.visitsData.created &&
                                !this.props.visitsData.creating && (
                                <button
                                    style={{ minWidth: '100%' }}
                                    className="button-validate"
                                    onClick={() => this.props._handleVisitCreation(Date.now())}
                                >
                                    <div className="row justified aligned">
                                        <span>Envoyer le rapport</span>
                                    </div>
                                </button>
                            )}
                            {!this.props.visitsData.created && this.props.visitsData.creating && <Loading />}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
Summary.propTypes = {}

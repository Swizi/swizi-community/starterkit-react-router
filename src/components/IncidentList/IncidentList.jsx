import React, { Component } from 'react'
import PropTypes from 'prop-types'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus'
import faList from '@fortawesome/fontawesome-free-solid/faListUl'

import IncidentPreview from '../IncidentPreview/IncidentPreview'

import './IncidentList.less'

export default class IncidentList extends Component {
    state = { incidents: [] }
    componentDidMount() {}
    render() {
        return (
            <div className="mainWrapper">
                <div className="new-visit-container column  centered aligned ">
                    <div className="header-container enterDown">
                        <img src="assets/images/logo_simple.png" alt="" />
                        <img
                            className="arrow-back"
                            src="https://png.icons8.com/ios/50/000000/left.png"
                            onClick={() => this.props.history.goBack()}
                        />
                    </div>
                    <span className="label-legend arriveSlide">
                        <FontAwesomeIcon icon={faList} className="icon-label" />Liste des incidents :
                    </span>
                    <div className="incidents-container arriveSlider">
                        {this.props.incidents.length ? (
                            this.props.incidents.map((incident, index) => {
                                return (
                                    <IncidentPreview
                                        key={index}
                                        data={incident}
                                        _newIncident={this.props._newIncident}
                                        isLast={this.props.incidents.length === index + 1}
                                    />
                                )
                            })
                        ) : (
                            <div className="no-incident arriveSlide">
                                Aucun incident déclaré
                                <div className="mini-button-add" onClick={() => this.props._newIncident()}>
                                    <FontAwesomeIcon icon={faPlus} size="2x" />
                                </div>
                            </div>
                        )}
                    </div>
                    <div className="last-item enterUp">
                        <button className="button-validate" onClick={() => this.props._endVisit()}>
                            <div className="row justified aligned">
                                <span>Terminer la visite</span>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

IncidentList.propTypes = {
    _endVisit: PropTypes.func.isRequired,
}

import React, { Component } from 'react'
import PropTypes from 'prop-types'

import moment from 'moment'
moment.locale('fr')

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faPhoto from '@fortawesome/fontawesome-free-solid/faImage'
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus'
import faRetro from '@fortawesome/fontawesome-free-solid/faCameraRetro'
import faList from '@fortawesome/fontawesome-free-solid/faListUl'
import faChevronDown from '@fortawesome/fontawesome-free-solid/faChevronDown'

import './Information.less'

export default class Information extends Component {
    state = { selectedValue: 0, selectedAllee: '', listAllee: [], selectClicked: false, alleName: '' }
    componentDidMount() {
        let listAllee = []
        this.props.visitsData.visits.filteredData.forEach(allee => {
            let option = {
                value: allee.id,
                label: `${allee.street_num} ${allee.street_name}, ${allee.street_postal} - ${allee.city}`,
            }
            listAllee.push(option)
        })
        this.setState({ listAllee })
    }
    handleChange = event => {
        this.setState({
            selectedValue: event.target.value,
            selectedAllee: this.state.listAllee[event.target.value].value,
            selectClicked: false,
            alleName: this.state.listAllee[event.target.value].label,
        })
    }
    render() {
        const value = this.state.selectedAllee && this.state.selectedAllee.value
        let pathname = this.props.history.location.pathname
        let action = this.props.history.action
        return (
            <div className="mainWrapper">
                <div className="new-visit-container column centered aligned">
                    <div
                        className={
                            pathname === '/information/incident'
                                ? 'header-container exitUp'
                                : action === 'POP'
                                    ? 'header-container enterDown'
                                    : 'header-container enterDown'
                        }
                    >
                        <img src="assets/images/logo_simple.png" alt="" />
                        <img
                            className="arrow-back"
                            src="https://png.icons8.com/ios/50/000000/left.png"
                            onClick={() => this.props._handleReset()}
                        />
                    </div>

                    <span className="label-legend">
                        <FontAwesomeIcon icon={faList} className="icon-label" />Liste des sites à visiter :
                    </span>
                    <div
                        className={
                            this.state.selectClicked
                                ? 'info-container select-container is-open'
                                : 'info-container select-container'
                        }
                    >
                        <select
                            value={this.state.selectedValue}
                            onChange={this.handleChange}
                            onFocus={() => this.setState({ selectClicked: true })}
                            onBlur={() => this.setState({ selectClicked: false })}
                        >
                            {this.state.listAllee.map((l, i) => {
                                return (
                                    <option key={i} value={i}>
                                        {l.label}
                                    </option>
                                )
                            })}
                        </select>
                        <FontAwesomeIcon className="rotatingIcon" icon={faChevronDown} size="2x" />
                    </div>
                    {this.state.selectedAllee && (
                        <span className="label-legend arriveSlide">
                            <FontAwesomeIcon icon={faRetro} className="icon-label" />Photo du site à visiter :
                        </span>
                    )}
                    {this.state.selectedAllee && (
                        <div
                            className="photo-container column aligned justified arriveSlide"
                            onClick={() => this.props._handleTakingMainPicture()}
                        >
                            {this.props.mainPicuture ? (
                                <img style={{ maxHeight: '30vh' }} src={this.props.mainPicuture} />
                            ) : (
                                <FontAwesomeIcon icon={faPhoto} size="10x" />
                            )}
                            <div className="mini-button-add">
                                <FontAwesomeIcon icon={faPlus} size="2x" />
                            </div>
                        </div>
                    )}
                    {this.state.selectedAllee &&
                        this.props.mainPicuture && (
                        <div className="last-item enterUp">
                            <button
                                className="button-validate"
                                onClick={() =>
                                    this.props._handleStartVisit(
                                        this.state.selectedAllee,
                                        Date.now(),
                                        this.state.alleName
                                    )
                                }
                            >
                                <div className="row justified aligned">
                                    <span>Démarrer la visite</span>
                                </div>
                            </button>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

Information.propTypes = {
    visitDate: PropTypes.number,
    _handleTakingMainPicture: PropTypes.func.isRequired,
    _handleStartVisit: PropTypes.func.isRequired,
}

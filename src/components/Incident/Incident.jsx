import React, { Component } from 'react'
import PropTypes from 'prop-types'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus'
import faPhoto from '@fortawesome/fontawesome-free-solid/faImage'
import faRetro from '@fortawesome/fontawesome-free-solid/faCameraRetro'
import faIncident from '@fortawesome/fontawesome-free-solid/faExclamationCircle'
import faDescription from '@fortawesome/fontawesome-free-solid/faAlignLeft'
import faLeftArrow from '@fortawesome/fontawesome-free-solid/faArrowLeft'
import faRightArrow from '@fortawesome/fontawesome-free-solid/faArrowRight'
import faChevronDown from '@fortawesome/fontawesome-free-solid/faChevronDown'

import './Incident.less'

export default class Incident extends Component {
    state = {
        photos: [],
        type: 0,
        description: '',
        options: [],
        selectedImg: 0,
        selectClicked: false,
        categories: null,
    }
    componentWillMount() {
        let options = []
        let uniqueType = []

        this.props.categories.forEach(categorie => {
            let hasProperty = false
            if (uniqueType.length) {
                uniqueType.forEach(c => {
                    if (c === categorie.type) {
                        hasProperty = true
                    } else {
                        uniqueType.push(categorie.type)
                    }
                })
            } else uniqueType.push(categorie.type)

            if (!hasProperty) {
                options.push({ label: [categorie.type], value: [categorie.type], disabled: true })
                options.push({ label: [categorie.nature], value: [categorie.id], disabled: false })
            } else {
                options.push({ label: [categorie.nature], value: [categorie.id], disabled: false })
            }
        })
        this.setState({ options, categories: this.props.categories }, () => console.log(this.state.options))
    }
    handleChange = event => {
        this.setState({ type: event.target.value })
    }
    _handleIncidentCreation = () => {
        console.log(this.state.categories)
        console.log(this.state.type)
        let indexInfos = this.state.categories.findIndex(c => c.id == this.state.type)
        this.props._handleIncidentCreation(
            Date.now(),
            this.state.photos,
            this.state.categories[indexInfos],
            this.state.description
        )
    }
    _handleDescriptionChange = description => {
        this.setState({ description })
    }
    prevPhoto = () => {
        let selectedImg = this.state.selectedImg
        if (this.state.selectedImg === 0) selectedImg = this.props.incidentPhotos.length - 1
        else selectedImg -= 1
        console.log(selectedImg)
        this.setState({ selectedImg })
    }
    nextPhoto = () => {
        let selectedImg = this.state.selectedImg
        if (this.state.selectedImg === this.props.incidentPhotos.length - 1) selectedImg = 0
        else selectedImg += 1
        console.log(selectedImg)
        this.setState({ selectedImg })
    }
    render() {
        return (
            <div className="mainWrapper">
                <div className="new-visit-container column  centered aligned ">
                    <div className="header-container enterDown">
                        <img src="assets/images/logo_simple.png" alt="" />
                        <img
                            className="arrow-back"
                            src="https://png.icons8.com/ios/50/000000/left.png"
                            onClick={() => this.props._handleBackToIncidents()}
                        />
                    </div>
                    <span className="label-legend arriveSlide">
                        <FontAwesomeIcon icon={faRetro} className="icon-label" />Photo(s) de l'incident :
                    </span>
                    <div className="photo-container column aligned justified arriveSlide" onClick={() => null}>
                        {this.props.incidentPhotos.length ? (
                            <img
                                style={{ maxHeight: '30vh' }}
                                src={this.props.incidentPhotos[this.state.selectedImg]}
                            />
                        ) : (
                            <FontAwesomeIcon icon={faPhoto} size="10x" />
                        )}
                        <div className="bubble-container">
                            {this.props.incidentPhotos.map((i1, i2) => {
                                return (
                                    <div
                                        key={i2}
                                        className={this.state.selectedImg === i2 ? 'selectedBubble' : 'bubble'}
                                    />
                                )
                            })}
                        </div>
                        {this.props.incidentPhotos.length > 1 && (
                            <div className="prev-photo column centered" onClick={() => this.prevPhoto()}>
                                <FontAwesomeIcon icon={faLeftArrow} />
                            </div>
                        )}
                        {this.props.incidentPhotos.length > 1 && (
                            <div className="next-photo column centered" onClick={() => this.nextPhoto()}>
                                <FontAwesomeIcon icon={faRightArrow} />
                            </div>
                        )}
                        {this.props.incidentPhotos.length < 3 && (
                            <div className="mini-button-add" onClick={() => this.props._handleTakingIncidentPicture()}>
                                <FontAwesomeIcon icon={faPlus} size="2x" />
                            </div>
                        )}
                    </div>
                    <span className="label-legend arriveSlide">
                        <FontAwesomeIcon icon={faIncident} className="icon-label" />Type d'incident :
                    </span>
                    <div
                        className={
                            this.state.selectClicked
                                ? 'info-container select-container is-open'
                                : 'info-container select-container'
                        }
                    >
                        {this.state.options.length && (
                            <select
                                value={this.state.type || 0}
                                onChange={this.handleChange}
                                onFocus={() => this.setState({ selectClicked: true })}
                                onBlur={() => this.setState({ selectClicked: false })}
                                placeholder="Selectionner la catégorie et sous-catégorie de l'incident"
                            >
                                {this.state.options.map((o, i) => {
                                    return (
                                        <option key={i} value={o.value} disabled={o.disabled}>
                                            {o.label}
                                        </option>
                                    )
                                })}
                            </select>
                        )}
                        <FontAwesomeIcon className="rotatingIcon" icon={faChevronDown} size="2x" />
                    </div>
                    {/* <Select
                        className="arriveSlide"
                        onInputChange={inputValue => (this._inputValue = inputValue)}
                        options={this.state.options}
                        onChange={this.handleChange}
                        value={this.state.type}
                        placeholder="Selectionner la catégorie et sous-catégorie de l'incident"
                    /> */}
                    <span className="label-legend arriveSlide">
                        <FontAwesomeIcon icon={faDescription} className="icon-label" />Description complémentaire de
                        l'incident :
                    </span>
                    <textarea
                        className="arriveSlide description-field"
                        value={this.state.description}
                        onChange={e => this._handleDescriptionChange(e.target.value)}
                    />
                    {!this.state.type && (
                        <div className="last-item enterUp" style={{ visibility: 'hidden' }}>
                            <button className="button-validate">
                                <div className="row justified aligned">
                                    <span>Créer l'incident</span>
                                </div>
                            </button>
                        </div>
                    )}
                    {this.state.photos &&
                        this.state.type && (
                        <div className="last-item enterUp">
                            <button className="button-validate" onClick={() => this._handleIncidentCreation()}>
                                <div className="row justified aligned">
                                    <span>Créer l'incident</span>
                                </div>
                            </button>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

Incident.propTypes = {}

import React from 'react'
import './IncidentPreview.less'
import PropTypes from 'prop-types'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus'

import moment from 'moment'
moment.locale('fr')

const IncidentPreview = props => {
    let hours = moment(props.data.dateCreation).format('LT')
    let category = props.data.infos.type + ' - ' + props.data.infos.nature
    let description = props.data.description
    return (
        <div className="incident-preview">
            <div className="date-incident">
                <span className="date-container">{hours}</span>
            </div>
            <div className="data-container column">
                <span className="category">{category}</span>
                {description && <span className="description">{description}</span>}
            </div>
            {props.isLast && (
                <div className="mini-button-add" onClick={() => props._newIncident()}>
                    <FontAwesomeIcon icon={faPlus} size="2x" />
                </div>
            )}
        </div>
    )
}
IncidentPreview.propTypes = {}
export default IncidentPreview

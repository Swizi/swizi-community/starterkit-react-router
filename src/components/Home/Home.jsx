import React from 'react'

import './Home.less'

import Loading from '../Loading/Loading'
import PropTypes from 'prop-types'

const Home = props => {
    let user = props.authData.user
    let pathname = props.history.location.pathname
    let action = props.history.action
    return (
        <div className="mainWrapper">
            <div className="home-container column centered aligned justified">
                <div
                    className={
                        pathname === '/information'
                            ? 'exitUp image-container'
                            : action === 'POP' ? 'enterDown image-container' : 'enterDown image-container'
                    }
                >
                    <img className="logo" src="assets/images/logo.png" alt="Logo de Opus67" />
                </div>
                {user.data === null || user.loading ? (
                    <div className="row aligned" style={{ minHeight: '70px' }}>
                        <Loading />
                    </div>
                ) : (
                    <div
                        className={
                            pathname === '/information'
                                ? 'button-wrapper exitDown'
                                : action === 'POP' ? 'enterUp button-wrapper' : 'button-wrapper'
                        }
                    >
                        {/* <div className="column justified centered aligned">
                            <div>
                                <b>{user.data.fullname}</b>
                            </div>
                            <div>
                                <i>N°ESO :</i> <b>{user.data.ESO}</b>
                            </div>
                        </div> */}
                        <button className="button-validate" onClick={() => props._initVisit()}>
                            <div className="row justified aligned">
                                <span>Initier une visite</span>
                                {/* <FontAwesomeIcon icon={faChevronRight} size="2x" /> */}
                            </div>
                        </button>
                    </div>
                )}
            </div>
        </div>
    )
}
Home.propTypes = {
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    authData: PropTypes.object.isRequired,
    _initVisit: PropTypes.func.isRequired,
}
export default Home

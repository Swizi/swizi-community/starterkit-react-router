import React from 'react'
import './Loading.less'

const Loading = () => (
    <div className="la-ball-square-clockwise-spin la-dark">
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
    </div>
)

export default Loading

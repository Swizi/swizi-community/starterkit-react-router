import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { HashRouter } from 'react-router-dom'

import './styles/styles.less'
import LOG from './utils/logger'

import AppContainer from './containers/AppContainer'

import configureStore from './redux/store'
const store = configureStore()

LOG('App starting in ' + NODE_ENV + ' mode', 'warn')

if (__ENABLE_DEVTOOLS__) {
    document.addEventListener('swiziReady', function() {
        render(
            <Provider store={store}>
                <HashRouter>
                    <AppContainer />
                </HashRouter>
            </Provider>,
            document.getElementById('root')
        )
    })
} else {
    render(
        <Provider store={store}>
            <HashRouter>
                <AppContainer />
            </HashRouter>
        </Provider>,
        document.getElementById('root')
    )
}

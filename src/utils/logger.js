export default (msg, type = 'log') => {
    if (__ENABLE_DEVTOOLS__) {
        console[type](msg)
    } else {
        if (typeof (msg) != 'string') msg = JSON.stringify(msg)
        swizi.log(type, msg)
    }
}